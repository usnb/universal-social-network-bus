# USNB additional documentation #

### User workshop questionnaire and results ###

- [Original questionnaire in French](https://gitlab.inria.fr/usnb/universal-social-network-bus/blob/master/documents/TestGroup17072018.pdf)
- [Translated questionnaire and answers in English](https://gitlab.inria.fr/usnb/universal-social-network-bus/blob/master/documents/Universal_Social_Network_Bus_SurveyAnswers_EN.pdf)

